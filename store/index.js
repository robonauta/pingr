export const state = () => ({
    email: '',
    password: '',
    username: '',
    token: ''
})

export const mutations = {
    setEmail(state, email) {
        state.email = email
    },
    setPassword(state, password) {
        state.password = password
    },
    setUsername(state, username) {
        state.username = username
    },
    setToken(state, token) {
        state.token = token
    }
}